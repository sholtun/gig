﻿using System;

namespace Common.Contracts
{
    public class MarketOddsDto
    {
        public string MarketId { get; set; }
        public string MarketName { get; set; }
        public Selection[] Selections { get; set; }
    }
    
    public class Selection
    {
        public int Id { get; set; }
        public SelectionType Type { get; set; }
        public string Name { get; set; }
        public float Parameter { get; set; }
        public float Price { get; set; }
    }

    
    public enum SelectionType
    {
        Over,
        Under
    }
}
