﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public class OddsFeedUrls
    {
        public OddsFeedUrls(string hostUri)
        {
            BaseUri = new Uri(hostUri);
        }

        public Uri BaseUri { get; }

        public Uri PublishOdds() => new Uri(BaseUri, $"api/odds");

        public Uri RemoveOdds(Guid marketId) => new Uri(BaseUri, $"api/odds/{marketId}");
    }
}
