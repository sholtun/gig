﻿using System;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Common.Protocol
{
    public class SubscriptionMiddleware<T>
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly Producer<T> _producer;

        public SubscriptionMiddleware(RequestDelegate next, ILogger<SubscriptionMiddleware<T>> logger, Producer<T> producer)
        {
            _next = next;
            _logger = logger;
            _producer = producer;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await _next(context);
                return;
            }

            using (var socket = await context.WebSockets.AcceptWebSocketAsync())
            {
                _logger.LogInformation("Consumer connected");
                try
                {
                    var ct = context.RequestAborted;
                    var subscriber = new Subscriber(socket);
                    using (await _producer.Subscribe(subscriber))
                    {
                        await WaitForCloseMessage(socket, ct);
                        _logger.LogInformation("Consumer disconnecting...");
                        await socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "", ct);
                    }
                }
                catch (OperationCanceledException)
                {
                    socket.Abort();
                }
                catch (WebSocketException)
                {
                    socket.Abort();
                }
                catch (SocketException)
                {
                    socket.Abort();
                }
                catch (Exception e)
                {
                    _logger.LogError(e,
                        $"Unexpected error. State: {socket.State}, status {socket.CloseStatus}, {socket.CloseStatusDescription}");
                    socket.Abort();
                }
                finally
                {
                    _logger.LogInformation("Consumer disconnected");
                }
            }
        }

        private async Task WaitForCloseMessage(WebSocket socket, CancellationToken ct)
        {
            WebSocketReceiveResult result;
            do
            {
                (_, result) = await socket.ReadMessage(ct);
                
            } while (!result.CloseStatus.HasValue);
        }
    }

    public class Subscriber : IDisposable
    {
        private readonly WebSocket _socket;
        public Guid Id { get; }

        public Subscriber(WebSocket socket)
        {
            _socket = socket;
            Id = Guid.NewGuid();
        }

        public void Dispose()
        {
            _socket?.Dispose();
        }

        public Task Publish(string message)
        {
            return _socket.SendMessage(message);
        }
    }
}