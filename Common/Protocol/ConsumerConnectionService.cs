﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;

namespace Common.Protocol
{
    public class ConsumerConnectionService<TConsumer, TDto> : BackgroundService where TConsumer : Consumer<TDto>
    {
        private readonly Consumer<TDto> _client;
        private readonly ILogger _logger;
        private readonly AsyncRetryPolicy<bool> _policy;
        private readonly ConsumerConfig _config;

        public ConsumerConnectionService(TConsumer client, IOptions<ConsumerConfig> config,
            ILogger<ConsumerConnectionService<TConsumer, TDto>> logger)
        {
            _config = config.Value;

            _client = client;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _policy = Policy
                .HandleResult(true).OrInner<Exception>()
                .RetryForeverAsync((result, count, waitFor) =>
                {
                    if (result.Exception != null)
                    {
                        logger.LogWarning(result.Exception, "Unhandled exception during message consuming");
                    }

                    logger.LogWarning("Reconnecting to {Uri}", _config.Uri);
                });
        }

        protected override Task ExecuteAsync(CancellationToken serviceExecutionCt)
        {
            _logger.LogInformation("Starting consumer. Connecting to {Uri}", _config.Uri);

            return _policy.ExecuteAsync(async () =>
            {
                try
                {
                    using (await _client.ConnectAsync(_config.Uri, serviceExecutionCt))
                    {
                        _logger.LogInformation("Connected to {uri}", _config.Uri);
                        var result = await _client.Completion;
                        _logger.LogWarning("Consumer disconnected", _config.Uri);
                        return result;
                    }
                }
                catch (Exception)
                {
                    await _client.OnConnectionLost();
                    throw;
                }
            });
        }

        public override async Task StopAsync(CancellationToken gracefulShutdownCt)
        {
            _logger.LogInformation("Disconnecting...");

            var stoppingExecution = base.StopAsync(gracefulShutdownCt);

            await _client.DisconnectAsync(gracefulShutdownCt);
            await stoppingExecution;

            _logger.LogInformation("Disconnected");
        }
    }

    
    public class ConsumerConfig
    {
        public string Uri { get; set; }
    }
}
