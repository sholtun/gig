﻿using System;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Protocol;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Common
{
    
    public abstract class Consumer<T>
    {
        private readonly ILogger _logger;
        private ClientWebSocket _socket;
        private TaskCompletionSource<bool> _completion;
        private Task _receivingTask;

        protected Consumer(ILogger<Consumer<T>> logger)
        {
            _logger = logger;
        }

        public Task<bool> Completion => _completion.Task;

        protected internal virtual Task OnConnectionLost()
        {
            _logger.LogWarning($"Connection was lost ");
            return Task.CompletedTask;
        }

        public virtual async Task<IDisposable> ConnectAsync(
            string uriString, CancellationToken serviceExecutionCt)
        {
            _completion = new TaskCompletionSource<bool>();
            _socket = new ClientWebSocket();
            
            var uri = new Uri(uriString);
            await _socket.ConnectAsync(uri, serviceExecutionCt);
            _receivingTask = ReceiveAsync(serviceExecutionCt);

            return _socket;
        }

        public virtual async Task DisconnectAsync(CancellationToken gracefulShutdownCt)
        {
            await OnConnectionLost();
            if (_socket.State != WebSocketState.Open)
                return;

            await _socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "normal closure from client side",
                gracefulShutdownCt);

            await _receivingTask;
        }

        // return value indicates should be reconnect launched or not. 
        // if true reconnect must be initiated
        private async Task ReceiveAsync(CancellationToken serviceExecutionCt)
        {
            while (_socket.State == WebSocketState.Open || _socket.State == WebSocketState.CloseSent)
            {
                var serializedMessage = string.Empty;
                try
                {
                    WebSocketReceiveResult result;
                    (serializedMessage, result) = await _socket.ReadMessage(serviceExecutionCt);

                    if (result.MessageType == WebSocketMessageType.Text)
                    {
                        var dto = JsonConvert.DeserializeObject<T[]>(serializedMessage);

                        await OnMessage(dto, serviceExecutionCt);
                    }
                    else if (result.MessageType == WebSocketMessageType.Close)
                    {
                        _logger.LogInformation("Close received");

                        var isShutdownInProgress = serviceExecutionCt.IsCancellationRequested;
                        if (!isShutdownInProgress)
                        {
                            await _socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "ok", serviceExecutionCt);
                        }
                    }
                }
                catch (TaskCanceledException)
                {
                    _logger.LogInformation("Service stopped");
                }
                catch (WebSocketException)
                {
                    _logger.LogInformation("Socket aborted");
                    _socket.Abort();
                }
                catch (SocketException)
                {
                    _logger.LogInformation("Socket aborted");
                    _socket.Abort();
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Unexpected exception. Message received: {message}", serializedMessage);
                    _socket.Abort();
                }
            }

            var isConnectionLost = _socket.State == WebSocketState.Aborted;
            if (isConnectionLost)
            {
                await OnConnectionLost();
            }
            _completion.SetResult(isConnectionLost);
        }

        protected abstract Task OnMessage(T[] message, CancellationToken ct);
    }
}