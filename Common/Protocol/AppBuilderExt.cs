﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Common.Protocol
{
    public static class AppBuilderExt
    {
        public static IServiceCollection AddConsumer<TMessage, TConsumer>(
            this IServiceCollection serviceCollection, IConfigurationSection config)
            where TConsumer : Consumer<TMessage>
        {   
            return serviceCollection
                .Configure<ConsumerConfig>(config.Bind)
                .AddSingleton<TConsumer>()
                .AddHostedService<ConsumerConnectionService<TConsumer, TMessage>>();
        }
        
        public static IServiceCollection AddProducer<TMessage, TStateProvider>(this IServiceCollection serviceCollection)
            where TStateProvider : class, IStateProvider<TMessage>
        {
            serviceCollection.AddSingleton<TStateProvider>();
            serviceCollection.AddSingleton<IStateProvider<TMessage>, TStateProvider>(sp => sp.GetService<TStateProvider>());
            serviceCollection.AddSingleton<Producer<TMessage>>();
            return serviceCollection;
        }
        
        public static IApplicationBuilder UseSubscriptionMiddleware<T>(this IApplicationBuilder app, PathString path)
        {
            return app.Map(path, a => a
                .UseWebSockets()
                .UseMiddleware<SubscriptionMiddleware<T>>());
        }
    }

    public interface IStateProvider<out T>
    {
        IEnumerable<T> GetState();
    }
}