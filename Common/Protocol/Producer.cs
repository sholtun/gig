﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Common.Protocol
{
    public class Producer<T>
    {
        private readonly IStateProvider<T> _stateProvider;
        private readonly ConcurrentDictionary<Guid, Subscriber> _subscribers = new ConcurrentDictionary<Guid, Subscriber>();
        private readonly JsonSerializerSettings _settings;

        public Producer(IStateProvider<T> stateProvider)
        {
            _stateProvider = stateProvider;
            _settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        public async Task<IDisposable> Subscribe(Subscriber subscriber)
        {
            _subscribers.GetOrAdd(subscriber.Id, subscriber);
            
            await SendInitialState(subscriber);

            return new Disposable(() =>
            {
                _subscribers.TryRemove(subscriber.Id, out var s);
                s.Dispose();
            });
        }

        private async Task SendInitialState(Subscriber subscriber)
        {
            var messages = _stateProvider.GetState();
            await subscriber.Publish(JsonConvert.SerializeObject(messages.ToArray(), _settings));
        }

        public Task Publish(params T[] message)
        {
            var serializedMessage = JsonConvert.SerializeObject(message, _settings);
            return Task.WhenAll(_subscribers.Values.Select(x => x.Publish(serializedMessage)));
        }
    }
}
