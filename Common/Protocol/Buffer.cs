﻿using System;
using System.Buffers;

namespace Common.Protocol
{
    public struct Buffer<T> : IDisposable
    {
        public Buffer(int size)
        {
            if (size <= 0) throw new ArgumentOutOfRangeException(nameof(size));

            Value = ArrayPool<T>.Shared.Rent(size);
            Size = size;
        }

        public T[] Value { get; }

        public int Size { get; }

        public void Dispose()
        {
            if (Value != null)
            {
                ArrayPool<T>.Shared.Return(Value, true);
            }
        }
    }

    public static class Buffer
    {
        public static Buffer<T> Create<T>(int size)
        {
            return new Buffer<T>(size);
        }
    }
}