﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Common.Contracts;

namespace Common.Protocol
{
    public class OddsStore : IStateProvider<MarketOddsDto>
    {
        private readonly ConcurrentDictionary<string, MarketOddsDto> _store =
            new ConcurrentDictionary<string, MarketOddsDto>();

        public void AddOrUpdateOdds(params MarketOddsDto[] dtos)
        {
            foreach (var dto in dtos)
            {
                _store.AddOrUpdate(dto.MarketId, dto, (key, odds) => dto);
            }
        }

        public void Remove(string marketId)
        {
            _store.TryRemove(marketId, out _);
        }

        public IEnumerable<MarketOddsDto> GetState()
        {
            return _store.Values;
        }
    }
}