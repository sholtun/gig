﻿using System;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Common.Protocol
{
    public static class WebSocketExt
    {

        public static async Task<(string text, WebSocketReceiveResult result)> ReadMessage(this WebSocket socket,
            CancellationToken ct)
        {
            string message;
            WebSocketReceiveResult result;

            using (var ms = new MemoryStream())
            {
                using (var rentedBuffer = Buffer.Create<byte>(1024 * 4))
                {
                    var buffer = new ArraySegment<byte>(rentedBuffer.Value);
                    do
                    {
                        result = await socket.ReceiveAsync(buffer, ct);
                        ms.Write(buffer.Array, buffer.Offset, result.Count);
                    } while (!result.EndOfMessage);

                    ms.Seek(0, SeekOrigin.Begin);
                }

                using (var reader = new StreamReader(ms, Encoding.UTF8))
                {
                    message = await reader.ReadToEndAsync().ConfigureAwait(false);
                }
            }

            return (message, result);
        }

        public static async Task SendMessage(this WebSocket socket, string message)
        {
            var encodedMessage = Encoding.UTF8.GetBytes(message);
            await socket.SendAsync(new ArraySegment<byte>(encodedMessage,
                    0,
                    encodedMessage.Length),
                WebSocketMessageType.Text,
                true,
                CancellationToken.None).ConfigureAwait(false);
        }
    }
}