﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Contracts;

namespace OddsFeed.Models
{
    public class PublishOddsRequest
    {
        public MarketOddsDto[] Odds { get; set; }
    }
}
