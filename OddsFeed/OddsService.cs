﻿using System.Threading.Tasks;
using Common;
using Common.Contracts;
using Common.Protocol;

namespace OddsFeed
{
    public class OddsService
    {
        private readonly Producer<MarketOddsDto> _producer;
        private readonly OddsStore _oddsStore;

        public OddsService(Producer<MarketOddsDto> producer, OddsStore oddsStore)
        {
            _producer = producer;
            _oddsStore = oddsStore;
        }

        public Task PublishOdds(MarketOddsDto[] dto)
        {
            _oddsStore.AddOrUpdateOdds(dto);
            return _producer.Publish(dto);
        }
    }
}