﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OddsFeed.Models;

namespace OddsFeed.Controllers
{
    [Route("api/odds")]
    [ApiController]
    public class OddsController : ControllerBase
    {
        private readonly OddsService _service;
        private readonly ILogger<OddsController> _logger;

        public OddsController(OddsService service, ILogger<OddsController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult> Publish([FromBody] PublishOddsRequest request)
        {
            try
            {
                await _service.PublishOdds(request.Odds);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error");
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        [HttpDelete("{marketId}")]
        public ActionResult Remove([FromRoute] Guid marketId)
        {
            try
            {
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error");
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
