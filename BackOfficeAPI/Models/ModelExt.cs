﻿using Common.Contracts;

namespace BackOfficeAPI.Models
{
    public static class ModelExt
    {
        public static MarketOddsDto ToDto(this OddsRequest request)
        {
            return new MarketOddsDto
            {
                MarketId = request.MarketId,
                MarketName = "AsianHandicap",
                Selections = new[]
                {
                    new Selection
                    {
                        Id = 1,
                        Name = "Over",
                        Parameter = request.OverParameter,
                        Price = request.OverPrice,
                        Type = SelectionType.Over
                    },
                    new Selection
                    {
                        Id = 2,
                        Name = "Under",
                        Parameter = request.UnderParameter,
                        Price = request.UnderPrice,
                        Type = SelectionType.Under
                    }
                }
            };
        }
    }
}
