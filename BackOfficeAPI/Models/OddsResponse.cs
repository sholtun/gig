﻿using Common.Contracts;

namespace BackOfficeAPI.Controllers
{
    public class OddsResponse
    {
        public MarketOddsDto[] Odds { get; set; }
    }
}