﻿using System;

namespace BackOfficeAPI.Models
{
    public class OddsRequest
    {
        public string MarketId { get; set; }
        public float OverParameter { get; set; }
        public float OverPrice { get; set; }

        public float UnderParameter { get; set; }
        public float UnderPrice { get; set; }
    }
}