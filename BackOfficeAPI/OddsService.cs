﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common.Contracts;
using Common.Protocol;

namespace BackOfficeAPI
{
    public class OddsService
    {
        private readonly OddsFeedClient _client;
        private readonly OddsStore _store;

        public OddsService(OddsFeedClient client, OddsStore store)
        {
            _client = client;
            _store = store;
        }
        public async Task PublishOdds()
        {
            await _client.PublishOdds(_store.GetState().ToArray());
        }

        public void RemoveOdds(string marketId)
        {
            _store.Remove(marketId);
        }

        public void AddOrUpdateOdds(MarketOddsDto dto)
        {
            _store.AddOrUpdateOdds(dto);
        }

        public MarketOddsDto[] GetOdds()
        {
            return _store.GetState().ToArray();
        }
    }
}