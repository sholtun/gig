﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using BackOfficeAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BackOfficeAPI.Controllers
{
    [Route("api/odds")]
    [ApiController]
    public class OddsController : ControllerBase
    {
        private readonly OddsService _service;
        private readonly ILogger<OddsController> _logger;

        public OddsController(OddsService service, ILogger<OddsController> logger)
        {
            _service = service;
            _logger = logger;
        }
        
        [HttpGet]
        public ActionResult<IEnumerable<OddsResponse>> GetOdds()
        {
            try
            {
                return Ok(new OddsResponse
                {
                    Odds = _service.GetOdds()
                });
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error");
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        
        [HttpPost]
        public ActionResult AddOrUpdateOdds([FromBody] OddsRequest request)
        {
            try
            {
                _service.AddOrUpdateOdds(request.ToDto());
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error");
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        
        [HttpDelete("{id}")]
        public ActionResult RemoveOdds(string id)
        {
            try
            {
                _service.RemoveOdds(id);
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error");
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
        
        [HttpPost("publish")]
        public async Task<ActionResult> Publish()
        {
            try
            {
                await _service.PublishOdds();
                return Ok();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Unexpected error");
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}
