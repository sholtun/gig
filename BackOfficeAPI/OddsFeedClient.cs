﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using BackOfficeAPI.Models;
using Common;
using Common.Contracts;

namespace BackOfficeAPI
{
    public class OddsFeedClient
    {
        private readonly HttpClient _httpClient;
        private readonly OddsFeedUrls _urls;

        public OddsFeedClient(HttpClient httpClient, OddsFeedUrls urls)
        {
            _httpClient = httpClient;
            _urls = urls;
        }

        public Task PublishOdds(MarketOddsDto[] request)
        {
            return _httpClient.PostAsync(_urls.PublishOdds(), new { Odds = request }, new JsonMediaTypeFormatter());
        }

        public Task RemoveOdds(Guid marketId)
        {
            return _httpClient.DeleteAsync(_urls.RemoveOdds(marketId));
        }
    }
}