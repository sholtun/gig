The system architecture is built with an assumption that each part may be scaled.

The system consists of 4 parts:
    1. OddsFeed - source of odds data. Can be replaced with some external odds provider. Exposes API to publish odds.
    2. PlayerAPI - exposes web socket endpoint to provide real-time odds for player.
    3. BackOfficeAPI - exposes API to add, update, remove odds. Uses OddsFeed client to publish updated odds.
    4. Common - contains common contracts and protocol part responsible for web socket connection.

Each service caches the current odds state in memory.

Possible real world scenario:

1. OddsFeed connects to a 3rd party odds provider and receives current events/markets/odds.
2. OddsFeed caches the state in memory.
3. PlayerAPI, BackofficeAPI connect to OddsFeed, receive odds state and cache it in memory.
4. Frontend part operates with odds cached in APIs and receive updates via web socket.

All odds updates from 3rd party providers are received by OddsFeed and propagated to other services via web socket connection.
For the test application all contract entities, dtos, etc. were placed in single assembly (Common). In real application they can be moved to separate assemlies and published as nuget packages.

The protocol part may be extended with message revisions to reduce message transition on reconnects.