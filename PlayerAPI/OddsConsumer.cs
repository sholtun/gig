﻿using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Contracts;
using Common.Protocol;
using Microsoft.Extensions.Logging;

namespace PlayerAPI
{
    public class OddsConsumer : Consumer<MarketOddsDto>
    {
        private readonly OddsStore _oddsStore;
        private readonly Producer<MarketOddsDto> _producer;

        public OddsConsumer(OddsStore oddsStore, Producer<MarketOddsDto> producer, ILogger<Consumer<MarketOddsDto>> logger) : base(logger)
        {
            _oddsStore = oddsStore;
            _producer = producer;
        }

        protected override Task OnMessage(MarketOddsDto[] message, CancellationToken ct)
        {
            _oddsStore.AddOrUpdateOdds(message);
            return _producer.Publish(message);
        }
    }
}